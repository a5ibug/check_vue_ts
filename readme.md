# 项目检测平台前端
## 使用模块
> 二维码生成组件
* qrcode.vue

> 顶部进度条
* nprogress 

> UI组件
* element-plus

> 异步请求
* axios

> 异步请求失败重试
* axios-retry

> 可视化表单设计
* vform3-builds

> 图表
* echarts

> 前端框架
* vue
* vue-router

> 构建工具
* vite

> 拼音转换
* pinyin

> 状态管理
* pinia

> css预处理器
* postcss

> 代码检查
* stylelint
* eslint

> 网页全屏
* screenfull

> 模糊搜索
* fuse.js

> css后处理
* autoprefixer

> css框架
* tailwindcss
![业务逻辑](./image/img_4.png)

## 生成报告流程
![生成报告流程](./image/img_2.png)

## 后台权限
![后台权限](./image/img.png)

## 委托单位权限
![委托单位权限](./image/img_1.png)

## 检测站权限
![检测站权限](./image/img_3.png)
