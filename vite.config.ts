import { UserConfigExport, ConfigEnv, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

const setAlias = (alias: [string, string][]) => alias.map(v => {return { find: v[0], replacement: path.resolve(__dirname, v[1]) }})

export default ({ command, mode }: ConfigEnv): UserConfigExport => {
    const root = process.cwd()
    const env = loadEnv(mode, root) as unknown as ImportMetaEnv
    return {
        resolve: {
            alias: setAlias([
                ['/@', 'src']
            ])
        },
        server: {
            port: env.VITE_PORT
        },
        build: {
            // sourcemap: true,
            // 去掉打包后的console
            terserOptions: {
                compress: {
                    drop_console: true
                }
            },
            manifest: true,
            rollupOptions: {
                output: {
                    manualChunks: {
                        'element-plus': ['element-plus'],
                        echarts: ['echarts'],
                        pinyin: ['pinyin']
                    }
                }
            },
            chunkSizeWarningLimit: 600
        },
        plugins: [
            vue(),
            createSvgIconsPlugin({
                // 指定需要缓存的图标文件夹
                iconDirs: [path.resolve(process.cwd(), 'src/icons')],
                // 指定symbolId格式
                symbolId: 'icon-[dir]-[name]'
            })
        ],
        css: {
            postcss: {
                plugins: [
                    require('postcss-nested'),
                    require('postcss-import'),
                    require('autoprefixer'),
                    require('postcss-simple-vars'),
                    require('tailwindcss/nesting'),
                    require('tailwindcss')
                ]
            }
        }
    }
}
