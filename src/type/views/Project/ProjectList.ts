import { IClientTableList } from '/@/type/views/Client/ClientList'
export type finish = '未完工' | '完工'
export interface IProjectTableList{
    id: number
    clients: Array<IClientTableList>
    address: string|null
    area: string|null
    baseKind: string|null
    buildUnit: string|null
    checkType: string|null
    clientId: number|null
    contracts: string|null
    createTime: string|null
    designUnit: string|null
    doingUnit: string|null
    endDate: string|null
    finish: finish
    finishArea: string|null
    item1: string|null
    item2: string|null
    item3: string|null
    item4: string|null
    item5: string|null
    jzr: string|null
    jzrdh: string|null
    jzrzh: string|null
    moniterUnit: string|null
    other: string|null
    progress: string|null
    project: string|null
    receiver: string|null
    regDate: string|null
    startDate: string|null
    structure: string|null
    syr: string|null
    tel: string|null
    testUnit: string|null
    updateTime: string|null
}
