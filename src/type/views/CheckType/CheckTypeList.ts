export interface ICheckTypeTableList {
    id: number
    reportName:string|null // 报告名称
    testCircle: string|null // 试验周期
    code: string|null // 报告代码
    form: string|null // 委托表单
    checkForm: string|null // 检测报告表单
    createTime: string|null // 创建时间
    updateTime: string|null // 更新时间
}
