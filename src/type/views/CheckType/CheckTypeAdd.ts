export interface ICheckTypeTableForm {
    reportName:string|null // 报告名称
    testCircle: string|null // 试验周期
    code: string|null // 报告代码
    form: string|null // 委托表单
    yssj: string|null // 原始数据
    checkForm: string|null // 检测报告表单
}
