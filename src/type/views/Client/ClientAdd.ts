export interface IClientForm {
    client: string
    contracts: string
    tel: string
    other: string
}
