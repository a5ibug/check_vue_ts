export interface IClientTableList {
    id: number
    client: string
    contracts: string|null
    tel: string|null
    other: string|null
    createTime: string|null
    updateTime: string|null
}
