import { IProjectTableList } from '/@/type/views/Project/ProjectList'
import { IClientTableList } from '/@/type/views/Client/ClientList'

interface report {
    id: number
    checkTypeId: number
    projectId: number
    /**
     * 委托报告id
     */
    samplesId: number

    /**
     * 检测站id
     */
    checkId: number

    /**
     * 接样日期
     */
    getTime: string

    /**
     * 实验日期
     */
    testTime: string


    /**
     * 送检形式
     */
    sendType: string

    order: string

    /**
     * 品总规格
     */
    specifications: string

    state: string

    /**
     * 试验编号|按规则,唯一
     */
    checkOrder: string

    /**
     * 使用部位
     */
    position: string

    /**
     * 送检部位
     */
    sendPosition: string

    /**
     * 备注
     */
    other: string

    /**
     * 检验人id
     */
    jy: number

    /**
     * 检验员签字时间
     */
    jyTime: string

    /**
     * 审核人id
     */
    sh: number

    /**
     * 审核员签字时间
     */
    shTime: string

    /**
     * 技术负责人id
     */
    jsfz: number

    jsfzTime: string

    createTime: string

    updateTime: string
    form: string
    jsonData: string
    checkForm: string
    checkJsonData: string
}
export interface IReportsDetails {
    report: report
    project: IProjectTableList
    client: IClientTableList
}
