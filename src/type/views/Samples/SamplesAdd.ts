export interface ISamplesAdd {
    check_id: number // 检测站id
    check_type_id: number // 检测类型id
    project_id: number // 项目id
    send_type: string // 送检类型
    // form: string // 表单数据 后端直接 添加的时候从check_type表取
    form_data:string // 委托单填写的数据
    order: string // 来样编号(委托单位自己填. 这个随便 委托单位做记录用)
    specifications: string // 品种规格
    other: string // 备注
    position: string // 使用部位
    send_position: string // 送检部位
}
