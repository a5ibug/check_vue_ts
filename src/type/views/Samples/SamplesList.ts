export interface ISamplesList {
    id: number
    checkTypeId: number
    projectId: number
    checkId: number
    getTime: string
    testTime: string
    sendType: string
    form: string
    formData: string
    specifications: string
    order: string
    state: string
    checkOrder: string
    position: string
    sendPosition: string
    other: string
    createTime: string
    updateTime: string
}
