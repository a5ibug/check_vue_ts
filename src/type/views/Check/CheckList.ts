export interface ICheckTableList {
    id: number
    name: string
    cma: string|null
    address: string|null
    lxr: string|null
    phone: string|null
    image: string|null
    createTime: string|null
    updateTime: string|null
}
