export interface IUserTableList {
    id: number
    pid: number
    user: string|null
    roleId: number
    inspectionStationId: number|null
    clientId: number|null
    createTime: string|null
    updateTime: string|null
    check: string|null
    client: string|null
}
