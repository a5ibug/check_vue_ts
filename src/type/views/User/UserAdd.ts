export interface IUserAdd {
    user: string
    password: string
    roleId: number
    image: string
}
