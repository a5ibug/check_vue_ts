export interface Role {
    id: number
    pid: number
    name: string|null
    value: string|null
    createTime: string|null
    updateTime: string|null
}
