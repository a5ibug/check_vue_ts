import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { IProjectBindsList } from '/@/type/views/User/ProjectBinds'

const api = {
    getListById: '/projectBinds/',
    editProjectBinds: '/projectBinds/'
}

export interface ITableList {
    page: number
    pageSize: number
}


/**
 * 根据id获取绑定列表
 * @param id
 * @return IResponse<IProjectBindsList>
 */
export function getProjectBindsById(id: number): Promise<AxiosResponse<IResponse<IProjectBindsList>>> {
    return request({
        url: `${api.getListById}${id}`,
        method: 'get'
    })
}

/**
 * 修改用户绑定项目
 */
export function editProjectBindIds(id:string,projectBindIds:Array<number>): Promise<AxiosResponse<IResponse>> {
    return request({
        url: `${api.editProjectBinds}${id}`,
        method: 'put',
        data:{
            projectBindIds
        }
    })
}
