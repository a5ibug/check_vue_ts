import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { IUserTableList } from '/@/type/views/User/UserList'
import { IUserAdd } from '/@/type/views/User/UserAdd'

const api = {
    getTableList: '/users/',
    addUser: '/users/',
    getRoles: '/users/roles/'
}

export interface ITableList {
    page: number
    pageSize: number
}

/**
 * 添加用户
 */
export function addUser(data: IUserAdd): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.addUser,
        method: 'post',
        data
    })
}
/**
 * 获取子账户权限
 */
export function getRoles(): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.getRoles,
        method: 'get'
    })
}
/**
 * 获取用户列表
 * @param tableList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}
