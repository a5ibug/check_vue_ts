import request from '/@/utils/request'
import { AxiosResponse } from 'axios'

const api = {
    upload: '/upload/file'
}

/**
 * 上传文件
 */
export function uploadFile(file: File): Promise<AxiosResponse<IResponse>> {
    console.log(file)
    const formData = new FormData()
    formData.append('file', file)
    console.log(formData)
    return request({
        url: api.upload,
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data' },
        data: formData
    })
}
