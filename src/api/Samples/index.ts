import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { ISamplesList } from '/@/type/views/Samples/SamplesList'
import { ISamplesAdd } from '/@/type/views/Samples/SamplesAdd'

const api = {
    getTableList: '/samples/',
    addSamples: '/samples/',
    getById: '/samples/',
    sendById: '/samples/get/' // 接样
}

export interface ITableList {
    page: number
    pageSize: number
}

export function sendSamplesById(id: string | string[]): Promise<AxiosResponse<IResponse<ISamplesList>>> {
    return request({
        url: api.sendById + id,
        method: 'get'
    })
}


export function getSamplesById(id: string | string[]): Promise<AxiosResponse<IResponse<ISamplesList>>> {
    return request({
        url: api.getById + id,
        method: 'get'
    })
}

/**
 * 线上委托检测
 */
export function addSamples(data: ISamplesAdd): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.addSamples,
        method: 'post',
        data
    })
}

/**
 * 获取检测类型列表
 * @param tableList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse<ISamplesList>>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}
