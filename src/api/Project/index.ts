import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { IProjectTableAdd } from '/@/type/views/Project/ProjectAdd'

const api = {
    getTableList: '/projects/',
    addProject: '/projects/'
}

export interface ITableList {
    page: number
    pageSize: number
}

/**
 * 添加委托单位
 */
export function addProject(data: IProjectTableAdd): Promise<AxiosResponse<IResponse>>
{
    return request({
        url: api.addProject,
        method: 'post',
        data
    })
}
/**
 * 获取委托单位列表
 * @param tableList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}
