import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { IReportsList } from '/@/type/views/Reports/ReportsList'
import { IReportsDetails } from '/@/type/views/Reports/ReportsDetails'

const api = {
    getTableList: '/reports/',
    getDetails: '/reports/',
    jianyan: '/reports/jianyan/',
    sh: '/reports/sh/',
    jsfz: '/reports/jsfz/'
}

export interface ITableList {
    page: number
    pageSize: number
}
/**
 * 提交检验数据
 * @param IReportsList
 */
export function jianyan(id:string,data: any): Promise<AxiosResponse<IResponse>>
{
    return request({
        url: `${api.jianyan}${id}/`,
        method: 'post',
        data
    })
}

/**
 * 审核
 */
export function sh(id:string,data: any): Promise<AxiosResponse<IResponse>>
{
    return request({
        url: `${api.sh}${id}/`,
        method: 'post',
        data
    })
}

/**
 * 技术负责
 */

export function jsfz(id:string,data: any): Promise<AxiosResponse<IResponse>>
{
    return request({
        url: `${api.jsfz}${id}/`,
        method: 'post',
        data
    })
}
/**
 * 获取检测单列表
 * @param IReportsList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse<ITable<IReportsList>>>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}

/**
 * 获取检测单列表
 * @param id
 */
export function getDetails(id:number): Promise<AxiosResponse<IResponse<IReportsDetails>>> {
    return request({
        url: `${api.getDetails}${id}/`,
        method: 'get'
    })
}

