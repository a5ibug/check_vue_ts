import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { IClientForm } from '/@/type/views/Client/ClientAdd'
import { loginParam } from '/@/api/layout'

const api = {
    getTableList: '/clients/',
    addClient: '/clients/',
    addClientUser: '/clients/user'
}

export interface ITableList {
    page: number
    pageSize: number
}

/**
 * 添加委托单位
 */
export function clientAdd(data: IClientForm): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.addClient,
        method: 'post',
        data
    })
}

/**
 * 添加委托单位和用户
 */
export function clientAndUserAdd(data: {client:IClientForm,user:loginParam}): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.addClientUser,
        method: 'post',
        data
    })
}
/**
 * 获取委托单位列表
 * @param tableList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}
