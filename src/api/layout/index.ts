import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { IMenubarList } from '/@/type/store/layout'
import { ICheckTableList } from '/@/type/views/Check/CheckList'
import { IClientTableList } from '/@/type/views/Client/ClientList'

// const api_url = 'http://127.0.0.1:8080'
const api = {
    login: '/auth/login',
    getUser: '/auth/getUser',
    getRouterList: '/api/User/getRoute'
}

export interface loginParam {
    user: string,
    password: string
}

export function login(param: loginParam):Promise<AxiosResponse<IResponse<string>>> {
    return request({
        url: api.login,
        method: 'post',
        data: param
    })
}

interface IGetuserRes {
    name: string
    role: string
    roleName: string,
    check: ICheckTableList | null,
    client: IClientTableList | null,
}

export function getUser(): Promise<AxiosResponse<IResponse<IGetuserRes>>> {
    return request({
        url: api.getUser,
        method: 'get'
    })
}

export function getRouterList(): Promise<AxiosResponse<IResponse<Array<IMenubarList>>>> {
    return request({
        url: api.getRouterList,
        method: 'get'
    })
}
