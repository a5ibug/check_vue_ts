import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { ICheckTypeTableList } from '/@/type/views/CheckType/CheckTypeList'
import { ICheckTypeTableForm } from '/@/type/views/CheckType/CheckTypeAdd'

const api = {
    getTableList: '/checkTypes/',
    addCheckType: '/checkTypes/',
    getById: '/checkTypes/'
}

export interface ITableList {
    page: number
    pageSize: number
}

/**
 * 添加检测类型
 */
export function addCheckType(data: ICheckTypeTableForm): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.addCheckType,
        method: 'post',
        data
    })
}
/**
 * 根据id获取单条详情
 * @param id
 * @return IResponse<ICheckTypeTableList>
 */
export function getCheckTypeById(id: number): Promise<AxiosResponse<IResponse<ICheckTypeTableList>>> {
    return request({
        url: `${api.getById}${id}`,
        method: 'get'
    })
}

/**
 * 获取检测类型列表
 * @param tableList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse<ICheckTypeTableList>>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}
