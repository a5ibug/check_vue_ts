import request from '/@/utils/request'
import { AxiosResponse } from 'axios'
import { ICheckTableList } from '/@/type/views/Check/CheckList'
import { loginParam } from '/@/api/layout'
import { ICheckForm } from '/@/type/views/Check/CheckAdd'

const api = {
    getTableList: '/checks/',
    addCheckUser: '/checks/user',
    getCheckType: '/checks/checkType/',
    editCheckType: '/checks/checkType/'
}

export interface ITableList {
    page: number
    pageSize: number
}

/**
 * 根据用户id查询检测类型
 */
export function getCheckType(id: string): Promise<AxiosResponse> {
    return request.get(`${api.getCheckType}${id}`)
}

/**
 * 修改检测站绑定委托单位
 */
export function editCheckType(id:string,checkTypeIds:Array<number>): Promise<AxiosResponse<IResponse>> {
    return request({
        url: `${api.editCheckType}${id}`,
        method: 'put',
        data:{
            checkTypeIds
        }
    })
}

/**
 * 添加检测站和用户
 */
export function CheckAndUserAdd(data: {check:ICheckForm,user:loginParam}): Promise<AxiosResponse<IResponse>> {
    return request({
        url: api.addCheckUser,
        method: 'post',
        data
    })
}
/**
 * 获取检测站列表
 * @param tableList
 */
export function getTableList(tableList: ITableList): Promise<AxiosResponse<IResponse<ITable<ICheckTableList>>>> {
    return request({
        url: api.getTableList,
        method: 'get',
        params: tableList
    })
}
