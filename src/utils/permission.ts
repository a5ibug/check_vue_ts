
// import router from '/@/router/index'
import { useLayoutStore } from '/@/store/modules/layout'
export type IPermissionType = 'or' | 'and'

export function checkPermission(permission:string|Array<string>, type:IPermissionType = 'or'):boolean {
    const { getUserInfo } = useLayoutStore()

    const value:Array<string> = typeof permission === 'string' ? [permission] : permission
    // const currentRoute = router.currentRoute.value
    // const roles:Array<string> = (currentRoute.meta.permission || []) as Array<string>
    const roles:string = getUserInfo.role
    // console.log(value,getUserInfo.role)
    const isShow = type === 'and'
        ? value.every(v => roles === v)
        : value.some(v => roles === v)
    return isShow
}
