import { defineStore } from 'pinia'
import { login, loginParam, getRouterList, getUser } from '/@/api/layout/index'
import {
    ILayout,
    IMenubarStatus,
    ITagsList,
    IMenubarList,
    ISetting,
    IMenubar,
    IStatus,
    ITags,
    IUserInfo
} from '/@/type/store/layout'
import router from '/@/router/index'
import { allowRouter } from '/@/router/index'
import { generatorDynamicRouter } from '/@/router/asyncRouter'
import { setLocal, getLocal, decode } from '/@/utils/tools'
import { RouteLocationNormalizedLoaded } from 'vue-router'
import { checkPermission } from '/@/utils/permission'

const setting = getLocal<ISetting>('setting')
const { ACCESS_TOKEN } = getLocal<IStatus>('token')

export const useLayoutStore = defineStore({
    id: 'layout',
    state: (): ILayout => ({
        menubar: {
            status: document.body.offsetWidth < 768 ? IMenubarStatus.PHN : IMenubarStatus.PCE,
            menuList: [],
            isPhone: document.body.offsetWidth < 768
        },
        // 用户信息
        userInfo: {
            name: '',
            role: '',
            check: null,
            client: null,
            roleName: ''
        },
        // 标签栏
        tags: {
            tagsList: [],
            cachedViews: []
        },
        setting: {
            theme: setting.theme !== undefined ? setting.theme : 0,
            showTags: setting.showTags !== undefined ? setting.showTags : true,
            color: {
                primary: setting.color !== undefined ? setting.color.primary : '#409eff'
            },
            usePinyinSearch: setting.usePinyinSearch !== undefined ? setting.usePinyinSearch : false,
            mode: setting.mode || 'vertical'
        },
        status: {
            isLoading: false,
            ACCESS_TOKEN: ACCESS_TOKEN || ''
        }
    }),
    getters: {
        getMenubar(): IMenubar {
            return this.menubar
        },
        getUserInfo(): IUserInfo {
            return this.userInfo
        },
        getTags(): ITags {
            return this.tags
        },
        getSetting(): ISetting {
            return this.setting
        },
        getStatus(): IStatus {
            return this.status
        }
    },
    actions: {
        changeCollapsed(): void {
            this.menubar.status = this.menubar.isPhone
                ? this.menubar.status === IMenubarStatus.PHN
                    ? IMenubarStatus.PHE
                    : IMenubarStatus.PHN
                : this.menubar.status === IMenubarStatus.PCN
                    ? IMenubarStatus.PCE
                    : IMenubarStatus.PCN
        },
        changeDeviceWidth(): void {
            this.menubar.isPhone = document.body.offsetWidth < 768
            this.menubar.status = this.menubar.isPhone ? IMenubarStatus.PHN : IMenubarStatus.PCE
        },
        // 切换导航，记录打开的导航
        changeTagNavList(cRouter: RouteLocationNormalizedLoaded): void {
            if (!this.setting.showTags) return // 判断是否开启多标签页
            // if(cRouter.meta.hidden && !cRouter.meta.activeMenu) return // 隐藏的菜单如果不是子菜单则不添加到标签
            if (new RegExp('^\/redirect').test(cRouter.path)) return
            const index = this.tags.tagsList.findIndex(v => v.path === cRouter.path)
            this.tags.tagsList.forEach(v => v.isActive = false)

            // 判断页面是否打开过
            if (index !== -1) {
                this.tags.tagsList[index].isActive = true
                return
            }
            // 这里有bug

            const tagsList: ITagsList = {
                name: cRouter.name as string,
                title: cRouter.meta.title as string,
                path: cRouter.path,
                isActive: true
            }
            this.tags.tagsList.push(tagsList)
        },
        removeTagNav(obj: { tagsList: ITagsList, cPath: string }): void {
            const index = this.tags.tagsList.findIndex(v => v.path === obj.tagsList.path)
            if (this.tags.tagsList[index].path === obj.cPath) {
                this.tags.tagsList.splice(index, 1)
                const i = index === this.tags.tagsList.length ? index - 1 : index
                this.tags.tagsList[i].isActive = true
                this.removeCachedViews({ name: obj.tagsList.name, index })
                router.push({ path: this.tags.tagsList[i].path })
            } else {
                this.tags.tagsList.splice(index, 1)
                this.removeCachedViews({ name: obj.tagsList.name, index })
            }
        },
        removeOtherTagNav(tagsList: ITagsList): void {
            const index = this.tags.tagsList.findIndex(v => v.path === tagsList.path)
            this.tags.tagsList.splice(index + 1)
            this.tags.tagsList.splice(0, index)
            this.tags.cachedViews.splice(index + 1)
            this.tags.cachedViews.splice(0, index)
            router.push({ path: tagsList.path })
        },
        removeAllTagNav(): void {
            this.tags.tagsList.splice(0)
            this.tags.cachedViews.splice(0)
            router.push({ path: '/redirect/' })
        },
        // 添加缓存页面
        addCachedViews(obj: { name: string, noCache: boolean }): void {
            if (!this.setting.showTags) return // 判断是否开启多标签页
            if (obj.noCache || this.tags.cachedViews.includes(obj.name)) return
            this.tags.cachedViews.push(obj.name)
        },
        // 删除缓存页面
        removeCachedViews(obj: { name: string, index: number }): void {
            // 判断标签页是否还有该页面
            if (this.tags.tagsList.map(v => v.name)
                .includes(obj.name)) return
            this.tags.cachedViews.splice(obj.index, 1)
        },
        logout(): void {
            this.status.ACCESS_TOKEN = ''
            localStorage.removeItem('token')
            history.go(0)
        },
        setToken(token: string): void {
            this.status.ACCESS_TOKEN = token
            setLocal('token', this.status, 30 * 24 * 60 * 60 * 1000)
        },
        setRoutes(data: Array<IMenubarList>): void {
            this.menubar.menuList = data
        },
        concatAllowRoutes(): void {
            allowRouter.reverse()
                .forEach(v => this.menubar.menuList.unshift(v))
        },
        // 修改主题
        changeTheme(num?: number): void {
            if (num === this.setting.theme) return
            if (typeof num !== 'number') num = this.setting.theme
            this.setting.theme = num
            localStorage.setItem('setting', JSON.stringify(this.setting))
        },
        // 修改主题色
        changeThemeColor(color: string): void {
            this.setting.color.primary = color
            localStorage.setItem('setting', JSON.stringify(this.setting))
        },
        changeTagsSetting(showTags: boolean): void {
            this.setting.showTags = showTags
            localStorage.setItem('setting', JSON.stringify(this.setting))

            if (showTags) {
                const index = this.tags.tagsList.findIndex(v => v.path === router.currentRoute.value.path)
                if (index !== -1) {
                    this.tags.tagsList.forEach(v => v.isActive = false)
                    this.tags.tagsList[index].isActive = true
                } else {
                    this.changeTagNavList(router.currentRoute.value)
                }
            }
        },
        changePinSearchSetting(showPinyinSearch: boolean): void {
            this.setting.usePinyinSearch = showPinyinSearch
            localStorage.setItem('setting', JSON.stringify(this.setting))
        },
        // 下次进去该页面刷新该页面(解决子页面保存之后，回到父页面页面不刷新问题)
        refreshPage(path: string): void {
            const name = this.tags.tagsList.filter(v => v.path === path)[0]?.name
            if (!name) return
            const index = this.tags.cachedViews.findIndex(v => v === name)
            this.tags.cachedViews.splice(index, 1)
        },
        changemenubarMode(mode: 'horizontal' | 'vertical'): void {
            this.setting.mode = mode
            localStorage.setItem('setting', JSON.stringify(this.setting))
        },
        async login(param: loginParam): Promise<void> {
            const res = await login(param)
            const token = res.data.data
            this.status.ACCESS_TOKEN = token
            setLocal('token', this.status, 30 * 24 * 60 * 60 * 1000)
            const { query } = router.currentRoute.value
            router.push(typeof query.from === 'string' ? decode(query.from) : '/')
        },
        async getUser(): Promise<void> {
            const res = await getUser()
            const userInfo = res.data.data
            this.userInfo.name = userInfo.name
            this.userInfo.role = userInfo.role
            this.userInfo.roleName = userInfo.roleName
            this.userInfo.check = userInfo.check
            this.userInfo.client = userInfo.client
        },

        GenerateRoutes: async function(): Promise<void> {
            const data: IMenubarList[] = [
                // {
                //     id: 999,
                //     parentId: 0,
                //     name: 'Permission',
                //     path: '/Permission',
                //     component: 'Layout',
                //     redirect: '/Test/Test',
                //     meta: { title: '表单生成', icon: 'el-icon-phone', alwaysShow: true }
                // },
                // {
                //     id: 9991,
                //     parentId: 999,
                //     name: 'Test',
                //     path: '/Test/Test',
                //     component: 'Test',
                //     meta: { title: '表单生成', icon: 'el-icon-goods' }
                // }
                // {
                //     id: 9992,
                //     parentId: 999,
                //     name: 'Test2',
                //     path: '/Test/Test2',
                //     component: 'Test2',
                //     meta: { title: '测试表单', icon: 'el-icon-goods' }
                // },
            ]
            // 判断权限根绝权限写死
            if (checkPermission('admin')) {
                /**
                 * 管理员权限
                 * ----
                 * 委托单位
                 * 检测站
                 * 检测报告
                 * 委托申请
                 * 账号管理
                 */
                const data2: IMenubarList[] = [
                    {
                        id: 1,
                        parentId: 0,
                        name: 'User',
                        path: '/User',
                        component: 'Layout',
                        redirect: '/User/UserList',
                        meta: { title: '账号管理', icon: 'el-icon-user', alwaysShow: true }
                    }, {
                        id: 11,
                        parentId: 1,
                        name: 'UserList',
                        path: '/User/UserList',
                        component: 'UserList',
                        meta: { title: '账号列表', icon: 'el-icon-goods' }
                    }, {
                        id: 2,
                        parentId: 0,
                        name: 'Project',
                        path: '/Project',
                        component: 'Layout',
                        redirect: '/Project/ProjectList',
                        meta: { title: '项目管理', icon: 'el-icon-phone' }
                    }, {
                        id: 20,
                        parentId: 2,
                        name: 'ProjectList',
                        path: '/Project/ProjectList',
                        component: 'ProjectList',
                        meta: { title: '项目列表', icon: 'el-icon-goods' }
                    }, {
                        id: 9,
                        parentId: 0,
                        name: 'Client',
                        path: '/Client',
                        component: 'Layout',
                        meta: { title: '委托单位', icon: 'el-icon-goods', alwaysShow: true }
                    }, {
                        id: 91,
                        parentId: 9,
                        name: 'ClientList',
                        path: '/Client/ClientList',
                        component: 'ClientList',
                        meta: { title: '委托单位列表', icon: 'el-icon-goods' }
                    }, {
                        id: 92,
                        parentId: 9,
                        name: 'ClientAdd',
                        path: '/Client/ClientAdd',
                        component: 'ClientAdd',
                        meta: { title: '添加委托单位', icon: 'el-icon-goods' }
                    }, {
                        id: 10,
                        parentId: 0,
                        name: 'Check',
                        path: '/Check',
                        component: 'Layout',
                        meta: { title: '检测站', icon: 'el-icon-goods', alwaysShow: true }
                    }, {
                        id: 101,
                        parentId: 10,
                        name: 'CheckList',
                        path: '/Check/CheckList',
                        component: 'CheckList',
                        meta: { title: '检测站列表', icon: 'el-icon-goods' }
                    }, {
                        id: 102,
                        parentId: 10,
                        name: 'CheckAdd',
                        path: '/Check/CheckAdd',
                        component: 'CheckAdd',
                        meta: { title: '添加检测站', icon: 'el-icon-goods' }
                    }, {
                        id: 3,
                        parentId: 0,
                        name: 'CheckType',
                        path: '/CheckType',
                        component: 'Layout',
                        meta: { title: '检测报告类型', icon: 'el-icon-goods', alwaysShow: true }
                    }, {
                        id: 31,
                        parentId: 3,
                        name: 'CheckTypeList',
                        path: '/CheckType/CheckTypeList',
                        component: 'CheckTypeList',
                        meta: { title: '检测报告类型列表', icon: 'el-icon-goods' }
                    }, {
                        id: 32,
                        parentId: 3,
                        name: 'CheckTypeFormDetail',
                        path: '/CheckType/CheckTypeFormDetail/:id',
                        component: 'CheckTypeFormDetail',
                        meta: {
                            title: '委托单详情',
                            icon: 'el-icon-goods',
                            activeMenu: '/CheckType/CheckTypeList',
                            hidden: true
                        }
                    },{
                        id: 32,
                        parentId: 3,
                        name: 'CheckTypeYSSJDetail',
                        path: '/CheckType/CheckTypeYSSJDetail/:id',
                        component: 'CheckTypeYSSJDetail',
                        meta: {
                            title: '原始数据详情',
                            icon: 'el-icon-goods',
                            activeMenu: '/CheckType/CheckTypeList',
                            hidden: true
                        }
                    }, {
                        id: 33,
                        parentId: 3,
                        name: 'CheckTypeCheckFormDetail',
                        path: '/CheckType/CheckTypeCheckFormDetail/:id',
                        component: 'CheckTypeCheckFormDetail',
                        meta: {
                            title: '检测报告单详情',
                            icon: 'el-icon-goods',
                            activeMenu: '/CheckType/CheckTypeList',
                            hidden: true
                        }
                    }, {
                        id: 34,
                        parentId: 3,
                        name: 'CheckTypeAdd',
                        path: '/CheckType/CheckTypeAdd',
                        component: 'CheckTypeAdd',
                        meta: { title: '检测类型添加', icon: 'el-icon-goods', activeMenu: '/CheckType/CheckTypeAdd' }
                    }, {
                        id: 6,
                        parentId: 0,
                        name: 'Samples',
                        path: '/Samples',
                        component: 'Layout',
                        redirect: '/Samples/SamplesList',
                        meta: { title: '委托管理', icon: 'el-icon-phone', alwaysShow: true }
                    }, {
                        id: 61,
                        parentId: 6,
                        name: 'SamplesList',
                        path: '/Samples/SamplesList',
                        component: 'SamplesList',
                        meta: { title: '委托列表', icon: 'el-icon-phone' }
                    }, {
                        id: 62,
                        parentId: 6,
                        name: 'SamplesDetails',
                        path: '/Samples/SamplesDetails/:id',
                        component: 'SamplesDetails',
                        meta: { title: '委托详情', icon: 'el-icon-phone', hidden: true }
                    }, {
                        id: 7,
                        parentId: 0,
                        name: 'Reports',
                        path: '/Reports',
                        component: 'Layout',
                        redirect: '/Reports/ReportsList',
                        meta: { title: '试验报告管理', icon: 'el-icon-phone', alwaysShow: true }
                    }, {
                        id: 71,
                        parentId: 7,
                        name: 'ReportsList',
                        path: '/Reports/ReportsList',
                        component: 'ReportsList',
                        meta: { title: '报告列表', icon: 'el-icon-phone' }
                    }, {
                        id: 72,
                        parentId: 7,
                        name: 'ReportsDetails',
                        path: '/Reports/ReportsDetails/:id',
                        component: 'ReportsDetails',
                        meta: { title: '报告详情', icon: 'el-icon-phone' ,hidden: true }
                    },{
                        id: 73,
                        parentId: 7,
                        name: 'ReportsPrint',
                        path: '/Reports/ReportsPrint/:id',
                        component: 'ReportsPrint',
                        meta: { title: '报告打印', icon: 'el-icon-phone' ,hidden: true }
                    }]
                data.push(...data2)
            }
            /**
             * 委托单位管理员权限
             */
            if (checkPermission('client_admin')) {
                /**
                 * 项目管理
                 *     - 项目列表
                 * 子账号管理
                 *     - 子账号列表
                 *     - 添加子账号
                 * 检测报告管理
                 *     - 检测报告列表
                 *     - 检测报告详情
                 * 委托申请管理
                 *     - 委托申请
                 *     - 委托列表
                 */
                const data3: IMenubarList[] = [{
                    id: 2,
                    parentId: 0,
                    name: 'Project',
                    path: '/Project',
                    component: 'Layout',
                    redirect: '/Project/ProjectList',
                    meta: { title: '项目管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 20,
                    parentId: 2,
                    name: 'ProjectList',
                    path: '/Project/ProjectList',
                    component: 'ProjectList',
                    meta: { title: '项目列表', icon: 'el-icon-goods' }
                }, {
                    id: 22,
                    parentId: 2,
                    name: 'ProjectAdd',
                    path: '/Project/ProjectAdd',
                    component: 'ProjectAdd',
                    meta: { title: '添加项目', icon: 'el-icon-goods' }
                }, {
                    id: 1,
                    parentId: 0,
                    name: 'User',
                    path: '/User',
                    component: 'Layout',
                    redirect: '/User/UserList',
                    meta: { title: '账号管理', icon: 'el-icon-user', alwaysShow: true }
                }, {
                    id: 11,
                    parentId: 1,
                    name: 'UserList',
                    path: '/User/UserList',
                    component: 'UserList',
                    meta: { title: '账号列表', icon: 'el-icon-goods' }
                }, {
                    id: 12,
                    parentId: 1,
                    name: 'UserAdd',
                    path: '/User/UserAdd',
                    component: 'UserAdd',
                    meta: { title: '添加子账号', icon: 'el-icon-goods' }
                }, {
                    id: 6,
                    parentId: 0,
                    name: 'Samples',
                    path: '/Samples',
                    component: 'Layout',
                    redirect: '/Samples/SamplesList',
                    meta: { title: '委托管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 61,
                    parentId: 6,
                    name: 'SamplesList',
                    path: '/Samples/SamplesList',
                    component: 'SamplesList',
                    meta: { title: '委托列表', icon: 'el-icon-phone' }
                }, {
                    id: 62,
                    parentId: 6,
                    name: 'SamplesDetails',
                    path: '/Samples/SamplesDetails/:id',
                    component: 'SamplesDetails',
                    meta: { title: '委托详情', icon: 'el-icon-phone', hidden: true }
                }, {
                    id: 63,
                    parentId: 6,
                    name: 'SamplesAdd',
                    path: '/Samples/SamplesAdd',
                    component: 'SamplesAdd',
                    meta: { title: '线上委托', icon: 'el-icon-phone' }
                }, {
                    id: 7,
                    parentId: 0,
                    name: 'Reports',
                    path: '/Reports',
                    component: 'Layout',
                    redirect: '/Reports/ReportsList',
                    meta: { title: '试验报告管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 71,
                    parentId: 7,
                    name: 'ReportsList',
                    path: '/Reports/ReportsList',
                    component: 'ReportsList',
                    meta: { title: '报告列表', icon: 'el-icon-phone' }
                }, {
                    id: 72,
                    parentId: 7,
                    name: 'ReportsDetails',
                    path: '/Reports/ReportsDetails/:id',
                    component: 'ReportsDetails',
                    meta: { title: '报告详情', icon: 'el-icon-phone' ,hidden: true }
                },{
                    id: 73,
                    parentId: 7,
                    name: 'ReportsPrint',
                    path: '/Reports/ReportsPrint/:id',
                    component: 'ReportsPrint',
                    meta: { title: '报告打印', icon: 'el-icon-phone' ,hidden: true }
                }
                ]
                data.push(...data3)
            }
            /**
             * 检测站管理员权限
             */
            if (checkPermission('check_admin')) {
                /**
                 * 委托申请管理
                 *     - 委托申请
                 *     - 委托列表
                 */
                const data4: IMenubarList[] = [{
                    id: 1,
                    parentId: 0,
                    name: 'User',
                    path: '/User',
                    component: 'Layout',
                    redirect: '/User/UserList',
                    meta: { title: '账号管理', icon: 'el-icon-user', alwaysShow: true }
                }, {
                    id: 11,
                    parentId: 1,
                    name: 'UserList',
                    path: '/User/UserList',
                    component: 'UserList',
                    meta: { title: '账号列表', icon: 'el-icon-goods' }
                }, {
                    id: 12,
                    parentId: 1,
                    name: 'UserAdd',
                    path: '/User/UserAdd',
                    component: 'UserAdd',
                    meta: { title: '添加子账号', icon: 'el-icon-goods' }
                }, {
                    id: 6,
                    parentId: 0,
                    name: 'Samples',
                    path: '/Samples',
                    component: 'Layout',
                    redirect: '/Samples/SamplesList',
                    meta: { title: '委托管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 61,
                    parentId: 6,
                    name: 'SamplesList',
                    path: '/Samples/SamplesList',
                    component: 'SamplesList',
                    meta: { title: '委托列表', icon: 'el-icon-phone' }
                }, {
                    id: 62,
                    parentId: 6,
                    name: 'SamplesDetails',
                    path: '/Samples/SamplesDetails/:id',
                    component: 'SamplesDetails',
                    meta: { title: '委托详情', icon: 'el-icon-phone', hidden: true }
                }, {
                    id: 7,
                    parentId: 0,
                    name: 'Reports',
                    path: '/Reports',
                    component: 'Layout',
                    redirect: '/Reports/ReportsList',
                    meta: { title: '试验报告管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 71,
                    parentId: 7,
                    name: 'ReportsList',
                    path: '/Reports/ReportsList',
                    component: 'ReportsList',
                    meta: { title: '报告列表', icon: 'el-icon-phone' }
                }, {
                    id: 72,
                    parentId: 7,
                    name: 'ReportsDetails',
                    path: '/Reports/ReportsDetails/:id',
                    component: 'ReportsDetails',
                    meta: { title: '报告详情', icon: 'el-icon-phone' ,hidden: true }
                },{
                    id: 73,
                    parentId: 7,
                    name: 'ReportsPrint',
                    path: '/Reports/ReportsPrint/:id',
                    component: 'ReportsPrint',
                    meta: { title: '报告打印', icon: 'el-icon-phone' ,hidden: true }
                }]
                data.push(...data4)
            }

            /**
             * 委托单位子用户权限
             */
            if (checkPermission('client_user')) {
                const data3: IMenubarList[] = [{
                    id: 2,
                    parentId: 0,
                    name: 'Project',
                    path: '/Project',
                    component: 'Layout',
                    redirect: '/Project/ProjectList',
                    meta: { title: '项目管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 20,
                    parentId: 2,
                    name: 'ProjectList',
                    path: '/Project/ProjectList',
                    component: 'ProjectList',
                    meta: { title: '项目列表', icon: 'el-icon-goods' }
                }, {
                    id: 6,
                    parentId: 0,
                    name: 'Samples',
                    path: '/Samples',
                    component: 'Layout',
                    redirect: '/Samples/SamplesList',
                    meta: { title: '委托管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 61,
                    parentId: 6,
                    name: 'SamplesList',
                    path: '/Samples/SamplesList',
                    component: 'SamplesList',
                    meta: { title: '委托列表', icon: 'el-icon-phone' }
                }, {
                    id: 62,
                    parentId: 6,
                    name: 'SamplesDetails',
                    path: '/Samples/SamplesDetails/:id',
                    component: 'SamplesDetails',
                    meta: { title: '委托详情', icon: 'el-icon-phone', hidden: true }
                }, {
                    id: 63,
                    parentId: 6,
                    name: 'SamplesAdd',
                    path: '/Samples/SamplesAdd',
                    component: 'SamplesAdd',
                    meta: { title: '线上委托', icon: 'el-icon-phone' }
                }, {
                    id: 7,
                    parentId: 0,
                    name: 'Reports',
                    path: '/Reports',
                    component: 'Layout',
                    redirect: '/Reports/ReportsList',
                    meta: { title: '试验报告管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 71,
                    parentId: 7,
                    name: 'ReportsList',
                    path: '/Reports/ReportsList',
                    component: 'ReportsList',
                    meta: { title: '报告列表', icon: 'el-icon-phone' }
                }, {
                    id: 72,
                    parentId: 7,
                    name: 'ReportsDetails',
                    path: '/Reports/ReportsDetails/:id',
                    component: 'ReportsDetails',
                    meta: { title: '报告详情', icon: 'el-icon-phone' ,hidden: true }
                },{
                    id: 73,
                    parentId: 7,
                    name: 'ReportsPrint',
                    path: '/Reports/ReportsPrint/:id',
                    component: 'ReportsPrint',
                    meta: { title: '报告打印', icon: 'el-icon-phone' ,hidden: true }
                }
                ]
                data.push(...data3)
            }
            /**
             * 接样员
             */
            if (checkPermission('check_jy')) {
                const data3: IMenubarList[] = [{
                    id: 6,
                    parentId: 0,
                    name: 'Samples',
                    path: '/Samples',
                    component: 'Layout',
                    redirect: '/Samples/SamplesList',
                    meta: { title: '委托管理', icon: 'el-icon-phone', alwaysShow: true }
                }, {
                    id: 61,
                    parentId: 6,
                    name: 'SamplesList',
                    path: '/Samples/SamplesList',
                    component: 'SamplesList',
                    meta: { title: '委托列表', icon: 'el-icon-phone' }
                }, {
                    id: 62,
                    parentId: 6,
                    name: 'SamplesDetails',
                    path: '/Samples/SamplesDetails/:id',
                    component: 'SamplesDetails',
                    meta: { title: '委托详情', icon: 'el-icon-phone', hidden: true }
                }
                ]
                data.push(...data3)
            }

            /**
             * 检测员 审核 技术负责
             */
            if (checkPermission('check_jianyan') || checkPermission('check_sh') || checkPermission('check_jsfz')) {
                const data5 = [
                    {
                        id: 7,
                        parentId: 0,
                        name: 'Reports',
                        path: '/Reports',
                        component: 'Layout',
                        redirect: '/Reports/ReportsList',
                        meta: { title: '试验报告管理', icon: 'el-icon-phone', alwaysShow: true }
                    }, {
                        id: 71,
                        parentId: 7,
                        name: 'ReportsList',
                        path: '/Reports/ReportsList',
                        component: 'ReportsList',
                        meta: { title: '报告列表', icon: 'el-icon-phone' }
                    }, {
                        id: 72,
                        parentId: 7,
                        name: 'ReportsDetails',
                        path: '/Reports/ReportsDetails/:id',
                        component: 'ReportsDetails',
                        meta: { title: '报告详情', icon: 'el-icon-phone' ,hidden: true }
                    },{
                        id: 73,
                        parentId: 7,
                        name: 'ReportsPrint',
                        path: '/Reports/ReportsPrint/:id',
                        component: 'ReportsPrint',
                        meta: { title: '报告打印', icon: 'el-icon-phone' ,hidden: true }
                    }
                ]
                data.push(...data5)
            }

            generatorDynamicRouter(data)
        }
    }
})
