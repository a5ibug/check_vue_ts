import { createApp } from 'vue'
import App from '/@/App.vue'
import ElementPlus from 'element-plus'
import direct from '/@/directive/index'
import router from '/@/router/index'
import { pinia } from '/@/store'
import '/@/permission'

import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/display.css'
import 'nprogress/nprogress.css'
import '/@/assets/css/index.css'
import 'virtual:svg-icons-register'
import SvgIcon from '/@/components/SvnIcon/index.vue'
import * as Icons from '@element-plus/icons'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const app = createApp(App)
direct(app)
app.use(ElementPlus)

app.use(router)
app.use(pinia)
app.component('SvgIcon', SvgIcon)
// 注册全局组件
Object.keys(Icons).forEach(key => {
    app.component(`elIcon${key}`, Icons[key as keyof typeof Icons])
})
app.component('elIconCirclePlusOutline', Icons.CirclePlus)


app.mount('#app')
